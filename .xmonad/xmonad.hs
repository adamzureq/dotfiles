import XMonad
import Data.Monoid
import System.Exit
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.DynamicLog
import XMonad.Layout.Spacing

import qualified XMonad.StackSet as W
import qualified Data.Map        as M

myTerminal = "alacritty"
myBrowser = "firefox"

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False

myBorderWidth = 3

myModMask = mod4Mask

myWorkspaces = [" www "," code "," doc "," game "," vbox "," ter "," gfx "," file "," chat "]

myNormalBorderColor = "#172b24"
myFocusedBorderColor = "#3f7561"

myKeys :: [(String, X ())]
myKeys = [
            --Xmonad--
             ("M-S-r", spawn "xmonad --recompile")
            ,("M-C-r", spawn "xmonad --restart")

            --My--
            ,("M-b", spawn myBrowser)
            ,("M-e e", spawn "emacsclient -c -a emacs")
            ,("M-<Return>", spawn myTerminal)
            ,("M-S-<Return>", spawn "dmenu_run")
            ,("M-e p", spawn "pavucontrol")

            --session managment--
            ,("M-q s", spawn "shutdown now")
            ,("M-q r", spawn "reboot")
            ,("M-q l", io (exitWith ExitSuccess))
         ]

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    ]

myLayout = avoidStruts(spacing 10 (tiled ||| Mirror tiled )) ||| Full
  where

     tiled   = Tall nmaster delta ratio

     nmaster = 1

     ratio   = 1/2

     delta   = 3/100

myManageHook = composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore ]

myEventHook = mempty


myStartupHook = do
  spawnOnce "emacs --daemon"
  spawnOnce "feh --randomize --bg-fill ~/Walls/dtos-backgrounds"
  spawnOnce "picom"

main = do
  mybar <- spawnPipe "xmobar -x 0 /home/zureq/.config/xmobar/xmobarrc"
  xmonad $ docks def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        mouseBindings      = myMouseBindings,
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        startupHook        = myStartupHook,
        logHook            = dynamicLogWithPP $ xmobarPP
          {
            ppOutput = hPutStrLn mybar
          , ppCurrent = xmobarColor "#cfac72" "" . wrap ("<box type=Bottom width=2 mb=2 color=#6e8a6b>") "</box>"
          , ppTitle = xmobarColor "#a38e7f" ""
          , ppSep = "     |    "
          }

    }`additionalKeysP` myKeys
     `removeKeysP`[
        "M-S-q"
        ]
